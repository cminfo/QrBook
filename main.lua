debug = true
math.randomseed(os.time())

local sti = require "sti"

local Glyphe = {}

local Player = {
  direction = {"up", "down", "left", "right", "upleft", "upright", "downleft", "downright", "nomove"},
  animation_down = {love.graphics.newImage('assets/player1.png'), love.graphics.newImage('assets/player2.png'), love.graphics.newImage('assets/player.png')},
  animation_up = {love.graphics.newImage('assets/playerBack1.png'), love.graphics.newImage('assets/playerBack2.png'), love.graphics.newImage('assets/playerBack.png')},
  animation_left = {love.graphics.newImage('assets/playerleft1.png'), love.graphics.newImage('assets/playerleft2.png'), love.graphics.newImage('assets/player.png')},
  animation_right = {love.graphics.newImage('assets/playerright1.png'), love.graphics.newImage('assets/playerright2.png'), love.graphics.newImage('assets/player.png')},
  pray1 = love.graphics.newImage('assets/playerpray1.png'),
  pray2 = love.graphics.newImage('assets/playerpray2.png'),
  cpt = 0,
  pt_life = 200,
  dead = false,
  score = 0
}

local Zombie = {
  animation_down = {love.graphics.newImage('assets/zombar1-a.png'), love.graphics.newImage('assets/zombar1-b.png'), love.graphics.newImage('assets/zombar1-a.png')},
  animation_up = {love.graphics.newImage('assets/zombarback1-a.png'), love.graphics.newImage('assets/zombarback1-b.png'), love.graphics.newImage('assets/zombarBack.png')},
  animation_left = {love.graphics.newImage('assets/zombarleft1-a.png'), love.graphics.newImage('assets/zombarleft1-b.png'), love.graphics.newImage('assets/zombarleft1-a.png')},
  animation_right = {love.graphics.newImage('assets/zombarright1-a.png'), love.graphics.newImage('assets/zombarright1-b.png'), love.graphics.newImage('assets/zombarright1-a.png')},
  animation_death = {love.graphics.newImage('assets/protch.png'), love.graphics.newImage('assets/flaque.png'), love.graphics.newImage('assets/flaque.png')},
  animation_fight = {love.graphics.newImage('assets/zombar1-aPaf1.png')},
  --cpt = 0,
  pt_life = 100,
  dead = false
}

cpt_dt = 0
zombieNumber = 2
bossNumber = 0
waveNumber = -1
currentZombiesNumber = 0
timer = os.time()


local Boss = {
  cpt =  0,
  pt_life = 500,
  dead = false,
}

function zombie_create(pdv, img, box)
  currentZombiesNumber = currentZombiesNumber + 1
  x = math.random(50, 1600 - 50)
  y = math.random(50, 900 - 50)

  local box2d = {
    body = love.physics.newBody(world, x, y, "dynamic"),
    shape = love.physics.newRectangleShape(box, box),
    fixture = fixture
  }

  box2d.fixture = love.physics.newFixture(box2d.body, box2d.shape)
  box2d.body:setUserData(0)

  newZombie = {
    img = img,
    direction = {"up", "down", "left", "right", "upleft", "upright", "downleft", "downright", "nomove"},
    cpt = 0,
    cpt_dt = 0,
    collision = 0,
    dir = "nomove",
    box2d = box2d,
    life = pdv,
    pt_life = pdv,
    m_dx = 0,
    m_dy = 0,
    dead = false,
    timer = 0
  }

    newZombie.update = function (zombie)
      local px = map.layers["Player"].objects[1].box2d.body:getX()
      local py = map.layers["Player"].objects[1].box2d.body:getY()
      local zx = zombie.box2d.body:getX()
      local zy = zombie.box2d.body:getY()

      zombie.box2d.body:setUserData(zombie.box2d.body:getUserData() + 1)

      zombie.m_dx = (px - zx) / math.abs(px - zx)
      zombie.m_dy = (py - zy) / math.abs(py - zy)
      zombie.cpt = zombie.cpt + 1

      local cst = 25
      if zombie.cpt % cst == 0 then
        if (box == 40) then
            if(zombie.m_dx == -1 and zombie.m_dy == -1) or (zombie.m_dx == 0 and zombie.m_dy == -1) or (zombie.m_dx == 1 and zombie.m_dy == -1) then
              zombie.img = Zombie.animation_up[zombie.cpt / cst]
            elseif(zombie.m_dx == -1 and zombie.m_dy == 1) or (zombie.m_dx == 0 and zombie.m_dy == 1) or (zombie.m_dx == 1 and zombie.m_dy == 1) then
              zombie.img = Zombie.animation_down[zombie.cpt / cst]
            elseif(zombie.m_dx == -1 and zombie.m_dy == 0) then
              zombie.img = Zombie.animation_left[zombie.cpt / cst]
            elseif(zombie.m_dx == 1 and zombie.m_dy == 0) then
              zombie.img = Zombie.animation_right[zombie.cpt / cst]
            elseif (zombie.m_dx == 0 and zombie.m_dy == 0) or (zombie.m_dx == nil and zombie.m_dy == nil)then
              zombie.img = Zombie.animation_up[3]
            end
        end
        if zombie.dead and zombie.cpt % cst == 0 then
          if os.time() - zombie.timer >= 1 then
            zombie.img = Zombie.animation_death[2]
          elseif os.time() - zombie.timer >= 0 then
            zombie.img = Zombie.animation_death[1]
          end
        end

      end


      if zombie.cpt / cst == 2 then
        zombie.cpt = 0
        if zombie.dead then
          table.remove(map.layers["Zombies"].objects, i)
          currentZombiesNumber = currentZombiesNumber - 1
        end

      end
    if (zombie.box2d.body:getUserData() > 20) then
      zombie.box2d.body:setLinearVelocity(px - zx, py - zy)
    end

    if(zombie.dead) then
      zombie.box2d.body:setLinearVelocity(0, 0)
    end

    zombie.cpt_dt = zombie.cpt_dt + 1
    if(zombie.cpt_dt % 60 == 0) then
      zombie_attack(zombie)
    end
  end

    -- LES ZOMBIES S'AFFICHENT PAS

  newZombie.draw = function (zombie)
   love.graphics.draw(zombie.img, zombie.box2d.body:getX() - 40, zombie.box2d.body:getY() - 30)
   love.graphics.draw(zombieRed, zombie.box2d.body:getX() - 200, zombie.box2d.body:getY() - 200)
  end

  table.insert(map.layers["Zombies"].objects, newZombie)

end


function beginContact(a, b, coll)
    if (a:getBody():getType() == "static") then
        tmp = a
        a = b
        b = tmp
    end
    local px = map.layers["Player"].objects[1].box2d.body:getX()
    local py = map.layers["Player"].objects[1].box2d.body:getY()
    local zx = a:getBody():getX()
    local zy = a:getBody():getY()
    vx, vy = b:getBody():getLinearVelocity()
    if (zx < px + 30 and zx > px - 30) then
        a:getBody():setLinearVelocity(-2 * vx, 0)
    elseif (zy < py + 30 and zy > py - 30) then
        a:getBody():setLinearVelocity(0, - 2 * vy)
    else
         math.random(50, 1600 - 50)

        a:getBody():setLinearVelocity(math.random(-400, 400), math.random(-400, 400))
    end
    a:getBody():setUserData(0)

end



function zombie_attack(zombie)
  local x = map.layers["Player"].objects[1].box2d.body:getX()
  local y = map.layers["Player"].objects[1].box2d.body:getY()
  local zx = zombie.box2d.body:getX()
  local zy = zombie.box2d.body:getY()
  if(zx <= x + 85 and zx >= x - 85) and (zy <= y + 60 and zy >= y - 60)  then
    love.audio.play(zSound.paf)
    zombie.img = Zombie.animation_fight[1]
    Player.pt_life = Player.pt_life - 10
    print(Player.pt_life)
  end
end



function love.load(arg)
    waveWait = os.time()
    ambients = {love.audio.newSource("assets/A1.mp3", "stream"), love.audio.newSource("assets/A2.mp3", "stream")}
    zSound = {love.audio.newSource("assets/Z1.mp3", "static"), love.audio.newSource("assets/Z2.mp3", "static"), love.audio.newSource("assets/Z3.mp3", "static"), paf = love.audio.newSource("assets/Paf.mp3", "static")}
    love.window.setFullscreen(true, "desktop")
    love.physics.setMeter(64)
    map = sti.new("assets/background.lua", {"box2d"})
    world = love.physics.newWorld(0, 0)
    world:setCallbacks(beginContact, endContact, preSolve, postSolve)
    map:box2d_init(world)
    Player.new(map)
    timer = 0
    rituel = false

    Img = {}
    table.insert(Img, love.graphics.newImage("assets/book1.png"))
    table.insert(Img, love.graphics.newImage("assets/book2.png"))
    table.insert(Img, love.graphics.newImage("assets/book3.png"))
    table.insert(Img, love.graphics.newImage("assets/book4.png"))
    table.insert(Img, love.graphics.newImage("assets/book5.png"))
    table.insert(Img, love.graphics.newImage("assets/book6.png"))

    zombieImg = love.graphics.newImage("assets/zombar1-a.png")
    bossImg = love.graphics.newImage("assets/boss1.png")

    book = {
        x = 610,
        y = 371,
        img = Img.img1,
        timer = 0,
        anim = 1
    }

    f_x = 0
    f_y = 0
    l_x = 0
    l_y = 0

    pause = false
    Shader = 0
    firstCanvas = 0
    secondCanvas = 0
    currentCanvas = 0
    otherCanvas = 0
    lastPoint = {x=0,y=0}
    fps = 0
    mouseParticles = {}
    c2 = 0
    sum = 0

    zombieRed = love.graphics.newImage("assets/red.png")
    blue = love.graphics.newImage("assets/blue.png")
    map:addCustomLayer("Zombies", 7)
    map.layers["Zombies"].objects = {}
  --for i = 0, zombieNumber, 1 do
    --zombie_create()
  --end
  local zombiesLayer = map.layers["Zombies"]
  function zombiesLayer:update(dt)
      for i, zombie in pairs(self.objects) do
          zombie.update(zombie)
        end
    end
  function zombiesLayer:draw()
       for i, zombie in pairs(self.objects) do
           zombie.draw(zombie)
        end
    end


    Glyphe.new()

    shaderPov = love.graphics.newShader[[
    extern number px; extern number py;
    vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
    {
        vec4 pixel = Texel(texture, texture_coords);
        vec4 old = pixel;
        number factor = 1 + ( pow(abs(screen_coords.x - px), 2) + pow(abs(screen_coords.y - py), 2) - pow(500.0, 2.0)) / pow(500.0, 2.0);
        pixel.r = pixel.r - pixel.r * factor;
        pixel.g = pixel.g - pixel.g * factor;
        pixel.b = pixel.b - pixel.b * factor;
        if (pow(abs(screen_coords.x - 610), 2) + pow(abs(screen_coords.y - 371), 2) <= pow(330.0, 2.0))
        {
            factor = 1 + ( pow(abs(screen_coords.x - 610), 2) + pow(abs(screen_coords.y - 371), 2) - pow(330.0, 2.0)) / pow(500.0, 2.0);
            pixel.r = max(pixel.r, old.r - old.r * factor);
            pixel.g = max(pixel.g, old.g - old.g * factor);
            pixel.b = max(pixel.b, old.b - old.b * factor);
        }
        return pixel;
    }
    ]]
end

function love.update(dt)

  if dt < 1/30 then
      love.timer.sleep(1/30 - dt)
  end

  while pause do
      if love.keyboard.isDown('p') or love.keyboard.isDown('escape') then
          pause = false
      end
      return
  end

  if (pause == false and player.dead == true) then
    love.event.push('quit')
  end


  local player = map.layers["Player"].objects[1]
    if player.rituel then
        if love.keyboard.isDown('lctrl') then
            dt = dt / 10
            player.img = Player.pray1

        elseif love.keyboard.isDown('lshift') then
            dt = 2 * dt
            if (Player.pt_life < 200) then

                Player.pt_life = Player.pt_life + 1
            end
            player.img = Player.pray2
        end
    end

  if(currentZombiesNumber == 0) then
    waveNumber = waveNumber + 1
    zombiesNumber = zombieNumber + 5 * waveNumber
    print("nbr", zombiesNumber)

    for i = 1, zombiesNumber, 1 do
        local r = math.random(0,10)
        if r == 1 then
            print("BOSS")
            zombie_create(500, bossImg, 80)
        else
          zombie_create(100, zombieImg, 40)
        end
    end
  end

  if ambients[1].isStopped then
    if ambients[2].isStopped then
      love.audio.play(ambients[1])
    end
  else love.audio.play(ambients[2])
  end

  x = love.mouse.getX()
  y = love.mouse.getY()
  if love.mouse.isDown(1) and rituel == false then
    f_x = x
    f_y = y
    rituel = true
    timer = os.time()
  end
  if love.mouse.isDown(1) == false and rituel then
    l_x = x
    l_y = y
    if os.time() - timer < 2 then
      symbol()
    end
    rituel = false
  end


  if love.keyboard.isDown('escape') then
      love.event.push('quit')
  end

  if love.keyboard.isDown('p') then
      pause = true
  end

    map:update(dt)
    world:update(dt)
    Glyphe.update(dt)

    for i, zombie in pairs(map.layers["Zombies"].objects) do
      if zombie.dead == true then --and os.time() - zombie.timer > 2 then
        zombie.box2d.body:destroy()
        table.remove(map.layers["Zombies"].objects, i)
        currentZombiesNumber = currentZombiesNumber - 1
      end
    end

    book.timer = book.timer + 1
    if (book.timer > 5) then
        book.anim = book.anim + 1
        book.timer = 0
        if (book.anim > 6) then
            book.anim = 1
        end
    end
    book.img = Img[book.anim]
end

function love.draw(dt)
    local x = map.layers["Player"].objects[1].box2d.body:getX()
    local y = map.layers["Player"].objects[1].box2d.body:getY()
    shaderPov:send("px", x)
    shaderPov:send("py", y)
    love.graphics.setShader(shaderPov)
    map:draw()
    if map.layers["Player"].objects[1].rituel then
        love.graphics.draw(blue, book.x - 200, book.y - 200)
    end
    love.graphics.draw(book.img, book.x, book.y)


   love.graphics.setColor(255, 255, 255, 255)
    Glyphe.draw()
    love.graphics.setShader()

    love.graphics.setColor(255,255,255)
    love.graphics.print(Player.pt_life, x - 10, y - 40)
    string = " Wave :".. waveNumber.." Zombies restant :"..currentZombiesNumber.." "
    love.graphics.print(string, 10, 10)
    love.graphics.print("Score "..Player.score, 10, 25)

    if Player.dead then
      player.img = love.graphics.newImage("assets/flaque.png")
      love.graphics.draw(love.graphics.newImage("assets/lost.png"), book.x-50, book.y)
    end


end


function symbol()
  if f_y <= l_y + 30 and f_y >= l_y - 30 then
    Player.attack(f_x, f_y - 50, l_x, l_y + 50)
    print("c est une ligne horizontale")
  end
  if f_x <= l_x + 30 and f_x >= l_x - 30 then
    Player.attack(f_x - 50, f_y, l_x + 50, l_y)
    print("c est une ligne verticale")
  end
end

function Player.new(map)
    player = map.layers["Player"].objects[1]
    player.img = love.graphics.newImage('assets/player.png')
    player.dir = Player.direction.nomove
    player.speed = 200
    cpt = 0
    player.pt_life = 200
    player.rituel = false

    map.layers["Player"].draw = function(self)
        love.graphics.draw(self.objects[1].img, self.objects[1].box2d.body:getX() + 40, self.objects[1].box2d.body:getY() + 30, 0, 1, 1, self.objects[1].img:getWidth(), self.objects[1].img:getHeight())
    end

    player.box2d = map.box2d_collision[1]
    player.box2d.body = love.physics.newBody(world, 100, 100, "dynamic")
    player.box2d.fixture:destroy()
    player.box2d.shape = love.physics.newRectangleShape(40, 40)
    player.box2d.fixture = love.physics.newFixture(player.box2d.body, player.box2d.shape)

    map.layers["Player"].update = function(self)
        player = self.objects[1]
        speed = player.speed
        if love.keyboard.isDown('left', 'q') and love.keyboard.isDown('up', 'z') then
          player.dir = "upleft"
          player.box2d.body:setLinearVelocity(-speed, -speed)
        elseif love.keyboard.isDown('left', 'q') and love.keyboard.isDown('down', 's') then
          player.dir = "downleft"
          player.box2d.body:setLinearVelocity(-speed, speed)
        elseif love.keyboard.isDown('right', 'd') and love.keyboard.isDown('down', 's') then
          player.dir = "downright"
          player.box2d.body:setLinearVelocity(speed, speed)
        elseif love.keyboard.isDown('right', 'd') and love.keyboard.isDown('up', 'z') then
          player.dir = "upright"
          player.box2d.body:setLinearVelocity(speed, -speed)
        elseif love.keyboard.isDown('left', 'q') then
          player.dir = "left"
          player.box2d.body:setLinearVelocity(-speed, 0)
        elseif love.keyboard.isDown('right', 'd') then
          player.dir = "right"
          player.box2d.body:setLinearVelocity(speed, 0)
        elseif love.keyboard.isDown('up', 'z') then
          player.dir = "up"
          player.box2d.body:setLinearVelocity(0, -speed)
        elseif love.keyboard.isDown('down', 's') then
          player.dir = "down"
          player.box2d.body:setLinearVelocity(0, speed)
        else
          player.dir = "nomove"
          player.box2d.body:setLinearVelocity(0, 0)
        end
        Player.anim(player)
        Player.death()

        if math.pow(player.box2d.body:getX() - book.x, 2) + math.pow(player.box2d.body:getY() - book.y, 2) <= math.pow(200, 2) then
            player.rituel = true
        else
            player.rituel = false
        end
    end
end

function Player.attack(finish_x, finish_y, start_x, start_y)
  for i, zombie in pairs(map.layers["Zombies"].objects) do
    zx = zombie.box2d.body:getX()
    zy = zombie.box2d.body:getY()
    print("Dans la fonction")
    print(zx,";",zy,"->",start_x,";",start_y,"->",finish_x,";",finish_y)

    if zx <= start_x and zx >= finish_x and zy <= start_y and zy >= finish_y then
      zombie.pt_life = zombie.pt_life - 50
      print("Sbim !!!")

      if zombie.pt_life <= 0 then
        zombie.dead = true
        Player.score = math.floor(zombie.life + Player.score + (math.abs(zombie.box2d.body:getX() - book.x) + math.abs(zombie.box2d.body:getY() - book.y)) / 100)
        zombie.timer = os.time()
      end
    end
  end
end





function Player.anim(player)
  Player.cpt = Player.cpt + 1
  cst = 15
  if Player.cpt % cst == 0 then
    if(player.dir == "up" or player.dir == "upleft" or player.dir == "upright") then
      player.img = Player.animation_up[Player.cpt / cst]
    elseif player.dir == "left" then
      player.img = Player.animation_left[Player.cpt / cst]
    elseif player.dir == "right" then
      player.img = Player.animation_right[Player.cpt / cst]
    elseif player.dir == "down" or player.dir == "downleft" or player.dir == "downright" then
      player.img = Player.animation_down[Player.cpt / cst]
    elseif player.dir == "nomove" then
      player.img = Player.animation_down[3]
    end
  end
  if Player.cpt / cst == 2 then
    Player.cpt = 0
  end
end

function Player.death()
  if(Player.pt_life <= 0) then
    Player.dead = true
    pause = true
  end
end

function Glyphe.new()
    Shader = love.graphics.newShader[[
        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
        {
            number pW = 1/love_ScreenSize.x;//pixel width
            number pH = 1/love_ScreenSize.y;//pixel height
            vec4 pixel = Texel(texture, texture_coords );//This is the current pixel
            vec2 coords = vec2(texture_coords.x-pW,texture_coords.y);
            vec4 Lpixel = Texel(texture, coords );//Pixel on the left
            coords = vec2(texture_coords.x+pW,texture_coords.y);
            vec4 Rpixel = Texel(texture, coords );//Pixel on the right
            coords = vec2(texture_coords.x,texture_coords.y-pH);
            vec4 Upixel = Texel(texture, coords );//Pixel on the up
            coords = vec2(texture_coords.x,texture_coords.y+pH);
            vec4 Dpixel = Texel(texture, coords );//Pixel on the down
            pixel.a += 10 * 0.0166667 * (Lpixel.a + Rpixel.a + Dpixel.a * 3 + Upixel.a - 6 * pixel.a);
            pixel.rgb = vec3(1.0,1.0,1.0);
            return pixel;
        }
    ]]

    firstCanvas = love.graphics.newCanvas()
	secondCanvas = love.graphics.newCanvas()

	currentCanvas = firstCanvas
	otherCanvas = secondCanvas

	lastPoint.x = love.mouse.getX()
	lastPoint.y = love.mouse.getY()
end

function Glyphe.update(dt)
---------Get average FPS
	c2 = c2 + 1;
	sum = sum + dt;
	if(sum > 1)then
		sum = sum / c2;
		fps = math.floor(1/sum);
		c2 = 0;
		sum = 0;
	end

	-----Add smoke
	if(love.mouse.isDown(1))then
		local x,y = love.mouse.getPosition()
        if y > 850 then
            return
        end
		local p = {};
		p.x = x; p.y = y;
		mouseParticles[#mouseParticles+1] = p;

		local dx = p.x - lastPoint.x;
		local dy = p.y - lastPoint.y;
		local dist = math.sqrt(dx * dx + dy * dy);

		---if there is a gap, fill it
		if(dist > 5)then
			local angle = math.atan2(dy,dx);
			local cosine = math.cos(angle);
			local sine = math.sin(angle)
			for i=1,dist,1 do
				local p2 = {};
				p2.x = lastPoint.x + i * cosine;
				p2.y = lastPoint.y + i * sine;
				mouseParticles[#mouseParticles+1] = p2;
			end
		end

		lastPoint.x = x; lastPoint.y = y;
	else
		--if mouse is up
		local x,y = love.mouse.getPosition()
		lastPoint.x = x; lastPoint.y = y;
	end
end

function Glyphe.draw()
	love.graphics.setColor(255,255,255,255)

	if(#mouseParticles > 0)then
		love.graphics.setCanvas(firstCanvas)
		love.graphics.setColor(255,255,255,255)
		for i=1,#mouseParticles do
			local p = mouseParticles[i];
			love.graphics.circle("fill",p.x,p.y,10);
		end
		love.graphics.setCanvas()
		mouseParticles = {}
		love.graphics.setColor(255,255,255,255)
	end


	love.graphics.setCanvas(otherCanvas)
	love.graphics.setShader(Shader)
	love.graphics.draw(currentCanvas)
	love.graphics.setShader()
	love.graphics.setCanvas()

	love.graphics.setCanvas(currentCanvas)
  love.graphics.clear()
	love.graphics.setCanvas()

	love.graphics.setCanvas(currentCanvas)
	love.graphics.setShader(Shader);
	love.graphics.draw(otherCanvas)
	love.graphics.setShader();
	love.graphics.setCanvas()

	love.graphics.draw(currentCanvas)

  love.graphics.setCanvas(otherCanvas)
  love.graphics.clear()
	love.graphics.setCanvas()
end
